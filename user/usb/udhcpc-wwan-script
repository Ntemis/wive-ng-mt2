#!/bin/sh

# Wive-NG udhcpc script

# include global config
. /etc/scripts/global.sh

[ -z "$1" ] && echo "Error: should be called from udhcpc" && exit 1

LOG="logger -t udhcpc-wwan"

#############################################################################################################################
RESOLV_CONF="/etc/resolv.conf"
RESOLV_CONF_WWAN="/tmp/resolv.conf.$interface"
RESOLV_CONF_BACK="/tmp/resolv.conf.$interface.tmp"
WWANIFNAME="/tmp/wwanif"

# Get DGW/MTU config
eval `nvram_buf_get 2860 MODEMMTU MODEMDGW`

if [ "$broadcast" != "" ]; then
    BROADCAST="broadcast $broadcast"
fi

if [ "$subnet" != "" ]; then
    NETMASK="netmask $subnet"
fi

if [ "$interface" != "" ]; then
    echo $interface > $WWANIFNAME
fi

case "$1" in
    deconfig)
	$LOG "All deconfig $interface."
	# disable forward for paranoid users
	sysctl -wq net.ipv4.conf.$interface.forwarding=0
	# generate random ip from zeroconfig range end set
	# this is hack for some ISPs checked client alive by arping
	rndip="169.254.$((RANDOM%253+1)).$((RANDOM%253+1))"
	ip -4 addr flush dev $interface
	ip -4 addr replace $rndip/32 dev $interface
	# never need down iface
	ip link set $interface up
	if [ -e /tmp/default.gw ]; then
	    olddgw=`tail -qn1 /tmp/default.gw`
	    $LOG "Restore gw to $olddgw via $wan_if for switch to cable"
	    ip -4 route replace default via $olddgw dev $wan_if
	fi
	if [ -e $RESOLV_CONF_BACK ]; then
	    $LOG "Restore $RESOLV_CONF from $RESOLV_CONF_BACK"
	    cp -f $RESOLV_CONF_BACK $RESOLV_CONF
	    service dnsserver reload
	fi
	rm -f $RESOLV_CONF_WWAN $RESOLV_CONF_BACK $WWANIFNAME
    ;;

    leasefail)
	$LOG "Lease fail $interface."
	# disable forward for paranoid users
	sysctl -wq net.ipv4.conf.$interface.forwarding=0
	rm -f $RESOLV_CONF_WWAN $WWANIFNAME
    ;;

    renew|bound)
    ########################################################################################################
    # IP/NETMASK/MTU
    ########################################################################################################
	$LOG "Renew ip adress $ip and $NETMASK for $interface from dhcp"
	ifconfig $interface $ip $BROADCAST $NETMASK

        if [ "$mtu" ] && [ "$MODEMMTU" = "AUTO" -o "$MODEMMTU" = "" ]; then
	    $LOG "Set MTU to $mtu bytes from dhcp server"
	    ip link set mtu $mtu dev $interface
	fi
    ########################################################################################################
    # DGW
    ########################################################################################################
	if [ "$router" != "" ]; then
	    $LOG "Deleting default route dev $interface"
	    while ip -4 route del default dev $interface ; do
	        :
	    done

	    # get DGW
	    metric=0
	    for raddr in $router ; do
		if [ "$metric" = "0" ]; then
		    first_dgw="$raddr"
		    if [ "$MODEMDGW" = "1" ]; then
			$LOG "Set fist default gateway to $first_dgw dev $interface via $first_dgw metric 0"
			ip -4 route replace default dev "$interface" via "$first_dgw" metric 0
		    else
			$LOG "Add low cost default route stub for $interface via $first_dgw"
			ip -4 route replace default dev "$interface" via "$first_dgw" metric 1000
		    fi
		fi
	    done

	    $LOG "Flush route cache"
	    ip -4 route flush cache
	fi

    ########################################################################################################
    # DNS/WINS and services
    ########################################################################################################
	rm -f $RESOLV_CONF_WWAN
	if  [ "$dns" != "" ]; then
	    $LOG "Renew DNS from dhcp $dns"
	    # parse dnsservers
	    for daddr in $dns ; do
		echo nameserver $daddr >> $RESOLV_CONF_WWAN
		if [ "$first_dgw" != "$daddr" ]; then
		    if [ "$first_dgw" != "" ]; then
			$LOG "Add static route to DNS $daddr dev $interface"
			ip -4 route replace "$daddr" via "$first_dgw" dev "$interface"
		    else
			ROUTE_NS=`ip -o -4 route get "$daddr" | cut -f -3 -d " "`
			if [ "$ROUTE_NS" != "" ] && [ "$daddr" != "$first_dgw" ]; then
			    $LOG "Add static route to DNS $ROUTE_NS dev $interface"
			    ip -4 route replace $ROUTE_NS dev $interface
            		fi
		    fi
		fi
	    done
	    if [ "$wan_static_dns" != "on" ]; then
		if [ ! -e $RESOLV_CONF_BACK ]; then
		    $LOG "Replace DNS from $interface to $RESOLV_CONF"
		    cp -f $RESOLV_CONF $RESOLV_CONF_BACK
		fi
		cp -f $RESOLV_CONF_WWAN $RESOLV_CONF
		service dnsserver reload
	    else
		$LOG "Do not touch $RESOLV_CONF, static DNS configured."
	    fi
	fi
	# read for all write by root
	chmod 644 "$RESOLV_CONF" > /dev/null 2>&1

    # rebuild shaper rules
    service shaper restart

    # rebuild firewall rules
    service iptables restart

    # enable forward for this if
    sysctl -wq net.ipv4.conf.$interface.forwarding=1

    ;;
esac
