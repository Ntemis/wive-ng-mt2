#!/bin/sh

# constants
LOG="logger -t modem helper($$)"

count=0
while [ -e /tmp/modem_helper ]; do
    if [ "$count" = "5" ]; then
	$LOG "Wait for pid at 25 seconds - exit modemhelper"
	rm -f /tmp/modem_helper
	exit 1
    fi
    count="$(($count+1))"
    sleep 5
done

echo $$ > /tmp/modem_helper

# get params
. /etc/scripts/global.sh

dhcpc_wwan() {
    if [ ! -z $1 ] && [ -e /proc/sys/net/ipv4/conf/$1 ]; then
	$LOG "Start udhcpc for $1"
	UDHCPCOPTS="-i $1 -S -R -T 5 -a -s /etc/scripts/udhcpc-wwan-script -p /var/run/udhcpc_$1.pid"
	(udhcpc $UDHCPCOPTS > /dev/null 2>&1) &
	touch /tmp/modem_ready
    fi
}

qmi_connect() {
    # switch to qmi mode
    QMI_PORT=`tail -qn1 /tmp/qmi_port | sed '!=.*!!'`
    CDC_PORT=`tail -qn1 /tmp/qmi_port | sed '!.*=!!'`
    wds=`uqmi -s -d "$device" --get-client-id wds`
    uqmi -s -d "$CDC_PORT" --set-client-id wds,"$wds" --start-network "$MODEMAPN" --autoconnect
    sleep 1

    if [ -e /proc/sys/net/ipv4/conf/$QMI_PORT ]; then
	ip link set $QMI_PORT up
	dhcpc_wwan $QMI_PORT
    else
	$LOG "iface $QMI_PORT not exist"
    fi
}

ncm_connect() {
    # switch to NCM mode
    if [ ! -z $MNGMTPORT ]; then
	$LOG "Start NCM connect with managment port $MNGMTPORT with APN $MODEMAPN"
	echo -ne "\r\nAT^NDISDUP=1,1,\"$MODEMAPN\"\r\n" > $MNGMTPORT
	sleep 1
    else
	$LOG "Managment port does not exist. Pure cdc_ether mode ?"
    fi

    if [ -e /proc/sys/net/ipv4/conf/$NCMIF ]; then
	ip link set $NCMIF up
	dhcpc_wwan $NCMIF
    else
	$LOG "iface $NCMIF not exist"
    fi
}

start() {
    # get local params
    get_param

    if [ "$MODEMENABLED" = "1" ] || [ "$MODEMAUTOCONNECT" = "1" ]; then
        $LOG "Start Modemhelper"
	if [ "$MODEMTYPE" = "3" ] && [ -e /tmp/qmi_port ]; then
	    # QMI MODE
	    qmi_connect
	elif [ "$MODEMTYPE" = "2" ] && [ -e /tmp/ncm_port ]; then
	    # NCM MODE
	    ncm_connect
	else
	    # PPP MODE
	    if [ -e /tmp/ncm_port ]; then
		if [ -e /proc/sys/net/ipv4/conf/$NCMIF ] && [ ! -z $MNGMTPORT ]; then
		    # switch to PPP mode
		    ip link set $NCMIF down
		    echo -ne '\r\nAT^NDISDUP=0,0,"internet"\r\n' > $MNGMTPORT
		    sleep 1
		fi
	    fi
	    if [ -e /var/run/ppp-$mdm_if.pid -o -e /var/run/$mdm_if.pid ]; then
		echo "pppd: already started!"
		rm -f /tmp/modem_helper
		exit 1
	    fi
    	    # if first start or power off/on need wait
    	    if [ ! -e /tmp/modem_ready ]; then
		$LOG "Wait device ready before connect"
		# set not first time flag
		touch /tmp/modem_ready
		# wait switch to stage 2
    		while [ ! -e /tmp/bootgood ]; do
	    	    # Sleep until file does exists/is created
	    	    sleep 3
		done
		# wait others services start or modem bootup
		sleep 15
	    fi
	    if [ ! -e "$MODEMPORT" ]; then
		$LOG "Device $MODEMPORT not exist!!! Please read log and check config."
		rm -f /tmp/modem_helper
		exit 1
	    fi
	    # generate new chat scripts and config files
	    mkdir -p $PPPDIR/peers
	    if [ "$MODEMTYPE" = "0" ]; then
		$LOG "Prepare chat-file for WCDMA/UMTS/GPRS modems"
		echo "	ABORT '~'
	    ABORT 'BUSY'
	    ABORT 'NO CARRIER'
	    ABORT 'ERROR'
	    REPORT 'CONNECT'
	    '' 'ATZ'" > $PPPDIR/peers/chat
	    if [ "$MODEMATENABLED" = "1" ]; then
	    if [ "$MODEMAT1" != "" ]; then
    		echo "	'' '$MODEMAT1'" >> $PPPDIR/peers/chat
	    fi
	    if [ "$MODEMAT2" != "" ]; then
		echo "	'' '$MODEMAT2'" >> $PPPDIR/peers/chat
	    fi
	    if [ "$MODEMAT3" != "" ]; then
		echo "	'' '$MODEMAT3'" >> $PPPDIR/peers/chat
	    fi
	    fi
		echo "	SAY 'Calling WCDMA/UMTS/GPRS'
		'' 'AT+CGDCONT=1,\"IP\",\"$MODEMAPN\"'
		'OK' 'ATD$MODEMDIALNUMBER'
		'CONNECT' ''" >> $PPPDIR/peers/chat
	    elif [ "$MODEMTYPE" = "1" ]; then
		$LOG "Prepare chat-file for CDMA/EVDO modems"
		echo "	ABORT '~'
		ABORT 'BUSY'
		ABORT 'NO CARRIER'
		ABORT 'ERROR'
		ABORT 'NO DIAL TONE'
		ABORT 'NO ANSWER'
		ABORT 'DELAYED'
		REPORT 'CONNECT'
		'' 'ATZ'" > $PPPDIR/peers/chat
		if [ "$MODEMATENABLED" = "1" ]; then
	    if  [ "$MODEMAT1" != "" ]; then
		echo "	'' '$MODEMAT1'" >> $PPPDIR/peers/chat
	    fi
	    if  [ "$MODEMAT2" != "" ]; then
		echo "	'' '$MODEMAT2'" >> $PPPDIR/peers/chat
	    fi
	    if [ "$MODEMAT3" != "" ]; then
		echo "	'' '$MODEMAT3'" >> $PPPDIR/peers/chat
	    fi
	    fi
		echo "	SAY 'Calling CDMA/EVDO'
		'' 'ATDT#777'
		'CONNECT' 'ATO'
		'' ''" >> $PPPDIR/peers/chat
	    fi

	    $LOG "Generate ppp options file"
	    echo "
	    $MODEMDEBUG
	    $MODEMPORT
	    $MODEMSPEED
	    linkname $mdm_if
	    crtscts
	    noipdefault
	    lock
	    ipcp-accept-local
	    lcp-echo-interval 60
	    lcp-echo-failure 6
	    $MODEMMTU
	    $MODEMMRU
	    usepeerdns
	    defaultroute
	    noauth
	    maxfail 0
	    holdoff 5
	    nodetach
	    persist
	    user $MODEMUSERNAME
	    password $MODEMPASSWORD
	    connect \"/usr/sbin/chat -s -S -V -t 60 -f $PPPDIR/peers/chat 2> $CHATLOG\"" > $OPTFILE

	    $LOG "Starting GSM_modem pppd..."
	    pppd call dialup $PPPDOPT & >> $CHATLOG
	fi
    fi
}

stop() {
    # PPP MODE
    # first send HUP for terminate connections and try some times
    # second send TERM for exit pppd process
    # if process not terminated send KILL
    if [ -e /var/run/ppp-$mdm_if.pid ]; then
	pid=`grep '^[0-9]\{1,\}$' /var/run/ppp-$mdm_if.pid`
    else
        pid=`ps w| awk '/pppd call dialup.*ppp_modem/ && !/awk/ { print $1; }'`
    fi

    if [ "$pid" != "" ]; then
	$LOG "Stopping modemhelper ppp"
	# close connection
        kill -SIGHUP "$pid"

	# terminate pppd
	count=0
        while kill $pid > /dev/null 2>&1; do
	    if [ "$count" = "3" ]; then
		kill -SIGKILL "$pid"  > /dev/null 2>&1
		count=0
		sleep 1
	    fi
	    count="$(($count+1))"
	    sleep 1
	done
	rm -f /var/run/ppp-$mdm_if.pid
	rm -f /var/run/$mdm_if.pid
	rm -f $PPPDIR/peers/chat
    fi

    # NCM MODE
    if [ -e /var/run/udhcpc_$NCMIF.pid ]; then
	pid=`cat /var/run/udhcpc_$NCMIF.pid`
	if [ "$pid" != "" ]; then
	    $LOG "Stopping modemhelper dhcp"
	    while kill $pid > /dev/null 2>&1; do
		# wait release procedure
		sleep 1
		kill -SIGKILL "$pid"  > /dev/null 2>&1
	    done
	fi
    fi
    ip link set $NCMIF down > /dev/null 2>&1
}

poweroffon() {
    if [ $CONFIG_RALINK_GPIO_PWR_USB != "0" ] && [ $CONFIG_RALINK_GPIO_PWR_USB != "" ]; then
	$LOG "USB Port power OFF."
	# usb gpio inverted, 4000 infinity
	gpio l "$CONFIG_RALINK_GPIO_PWR_USB" 4000 0 0 0 0
	rm -f /tmp/modem_ready
    fi
    sleep 3
    if [ "$CONFIG_RALINK_GPIO_PWR_USB" != "0" ] && [ "$CONFIG_RALINK_GPIO_PWR_USB" != "" ]; then
	$LOG "USB Port power ON and wait for modem bootup."
	# usb gpio inverted, 4000 infinity
	gpio l "$CONFIG_RALINK_GPIO_PWR_USB" 0 4000 0 0 0
    fi
}

get_param() {
    # get local param always
    eval `nvram_buf_get 2860 WMODEMPORT MODEMSPEED MODEMUSERNAME \
	MODEMPASSWORD MODEMDIALNUMBER MODEMMTU MODEMAPN MODEMATENABLED MODEMAT1 MODEMAT2 MODEMAT3`

    # check base needed parametrs
    if [ "$MODEMTYPE" = "" ]; then
	rm -f /tmp/modem_helper
	exit 1
    fi

    # need add auto detect logic
    if [ -e /dev/pcui ]; then
	MNGMTPORT="/dev/pcui"
    fi

    if [ "$MODEMTYPE" = "2" ]; then
	# NCM MODE
	if [ ! -e /tmp/ncm_port ]; then
	    $LOG "Not found NCM port"
	    rm -f /tmp/modem_helper
	    exit 1
	fi
	NCMIF=`tail -qn1 /tmp/ncm_port`
    fi


    if [ "$MODEMTYPE" = "0" ] && [ "$MODEMDIALNUMBER" = "" ]; then
	 MODEMDIALNUMBER="*99#"
    fi

    # select device and exist check
    if [ "$WMODEMPORT" = "AUTO" ]; then
	MODEMPORT="/dev/modem"
    else
	MODEMPORT="/dev/$WMODEMPORT"
    fi

    # correct others params
    if [ "$MODEMAPN" = "" ]; then
	MODEMAPN="internet"
    fi

    if [ "$MODEMUSERNAME" = "" ]; then
	MODEMUSERNAME="internet"
    fi

    if [ "$MODEMPASSWORD" = "" ]; then
	MODEMPASSWORD="internet"
    fi

    if [ "$MODEMSPEED" = "AUTO" ]; then
	MODEMSPEED=""
    fi

    if [ "$MODEMMTU" = "" ] || [ "$MODEMMTU" = "AUTO" ]; then
        MODEMMRU=""
        MODEMMTU=""
    else
        MODEMMRU="mru $MODEMMTU"
        MODEMMTU="mtu $MODEMMTU"
    fi
    if [ "$MODEMDEBUG" = "1" ]; then
        MODEMDEBUG="debug"
    else
	MODEMDEBUG=""
    fi

    PPPDIR="/etc/ppp"
    OPTFILE="$PPPDIR/peers/dialup"
    CHATLOG="/var/log/chat.log"
    PPPDOPT="file $OPTFILE ifname $mdm_if $MODEMDEBUG -detach"
}

case "$1" in
	start)
	    start
	    ;;

	stop)
	    stop
	    ;;

	restart)
	    stop
	    start
	    ;;
	poweroffon)
	    poweroffon
	    ;;
	*)
	    echo $"Usage: $0 {start|stop|restart|poweroffon}"
esac

# allways remove run lock flag
rm -f /tmp/modem_helper
