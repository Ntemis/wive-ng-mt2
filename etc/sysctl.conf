kernel.panic = 1
kernel.panic_on_oops = 1
kernel.print-fatal-signals = 0

kernel.tainted = 0
kernel.printk = 6 4 1 6
kernel.randomize_va_space = 0
kernel.sched_child_runs_first = 1

vm.panic_on_oom = 1
vm.min_free_kbytes = 4096
vm.pagecache_ratio = 5
vm.overcommit_memory = 2
vm.overcommit_ratio = 100
vm.vfs_cache_pressure = 2000
vm.dirty_background_ratio = 5
vm.dirty_expire_centisecs = 1000
vm.dirty_writeback_centisecs = 100

#########################################################
# increase entropy pool size and allow more entropy commit in one time
kernel.random.read_wakeup_threshold = 512
kernel.random.write_wakeup_threshold = 2048
#########################################################

net.ipv4.conf.all.accept_redirects = 0
net.ipv4.conf.all.accept_source_route = 0
net.ipv4.conf.all.secure_redirects = 0
net.ipv4.conf.all.disable_xfrm = 1
net.ipv4.conf.all.disable_policy = 1
net.ipv4.conf.default.accept_redirects = 0
net.ipv4.conf.default.secure_redirects = 0
net.ipv4.conf.default.disable_xfrm = 1
net.ipv4.conf.default.disable_policy = 1

net.ipv4.icmp_echo_ignore_broadcasts = 1
net.ipv4.icmp_ignore_bogus_error_responses = 1
net.ipv4.igmp_max_memberships = 64

net.ipv4.inet_peer_maxttl = 128
net.ipv4.inet_peer_minttl = 1

#########################################################
# route cache farbage collector tune
net.ipv4.route.gc_elasticity = 4
net.ipv4.route.gc_interval = 40
net.ipv4.route.gc_min_interval = 2
net.ipv4.route.gc_timeout = 60

net.ipv6.route.gc_elasticity = 4
net.ipv6.route.gc_interval = 40
net.ipv6.route.gc_min_interval = 2
net.ipv6.route.gc_timeout = 60
#########################################################

#########################################################
# arp solisit and cache tune
net.ipv4.neigh.default.delay_first_probe_time = 2
net.ipv4.neigh.default.gc_stale_time = 480
net.ipv4.neigh.default.gc_interval = 20
net.ipv4.neigh.default.gc_thresh1 = 512
net.ipv4.neigh.default.gc_thresh2 = 2048
net.ipv4.neigh.default.gc_thresh3 = 4096

net.ipv4.neigh.default.mcast_solicit = 4
net.ipv4.neigh.default.mcast_resolicit = 2

net.ipv6.neigh.default.delay_first_probe_time = 2
net.ipv6.neigh.default.gc_stale_time = 480
net.ipv6.neigh.default.gc_interval = 20
net.ipv6.neigh.default.gc_thresh1 = 512
net.ipv6.neigh.default.gc_thresh2 = 2048
net.ipv6.neigh.default.gc_thresh3 = 4096

net.ipv6.neigh.default.mcast_solicit = 4
net.ipv6.neigh.default.mcast_resolicit = 2
#########################################################

#########################################################
net.ipv4.route.mtu_expires = 300
net.ipv4.tcp_mtu_probing = 1
net.ipv4.tcp_tso_win_divisor = 20
net.ipv4.tcp_max_syn_backlog = 256
#########################################################

#########################################################
net.ipv4.route.max_size = 16384
net.ipv6.route.max_size = 16384
net.ipv6.conf.all.accept_ra = 0
net.ipv6.conf.default.accept_ra = 0
net.ipv6.conf.all.ndisc_notify = 1
net.ipv6.conf.default.ndisc_notify = 1
#########################################################

#########################################################
# disable frto or use SACK enhanced version
# prevent bug TCP: snd_cwnd is null
# http://patchwork.ozlabs.org/patch/234793/
net.ipv4.tcp_frto = 2
#########################################################

net.ipv4.tcp_dsack = 1
net.ipv4.tcp_fack = 1
net.ipv4.tcp_fin_timeout = 15
net.ipv4.tcp_orphan_retries = 1
net.ipv4.tcp_low_latency = 1
net.ipv4.tcp_adv_win_scale = 0

net.ipv4.tcp_keepalive_intvl = 20
net.ipv4.tcp_keepalive_probes = 3
net.ipv4.tcp_keepalive_time = 300

net.ipv4.tcp_no_metrics_save = 1
net.ipv4.tcp_reordering = 192
net.ipv4.tcp_retries1 = 2
net.ipv4.tcp_retries2 = 4
net.ipv4.tcp_rfc1337 = 1
net.ipv4.tcp_sack = 0
net.ipv4.tcp_window_scaling = 0
net.ipv4.tcp_timestamps = 0
net.ipv4.tcp_tw_recycle = 1
net.ipv4.tcp_tw_reuse = 1
net.ipv4.tcp_workaround_signed_windows = 0
net.ipv4.tcp_max_orphans = 512
net.ipv4.tcp_max_tw_buckets = 4096

#########################################################
# prevent SegmentSmack DOS
#########################################################
net.ipv4.ipfrag_high_thresh = 262144
net.ipv4.ipfrag_low_thresh = 196608
net.ipv6.ip6frag_high_thresh = 262144
net.ipv6.ip6frag_low_thresh = 196608

#########################################################
# increase buffers sizes
#########################################################
net.ipv4.tcp_mem = 512 2048 16384
net.ipv4.udp_mem = 512 2048 16384
net.core.wmem_max = 2097152
net.core.rmem_max = 4194304

#########################################################
# rpfilter and arp_announce
# need enable for correct work dhcp
# client over wifi or vlan based WAN
#########################################################
net.ipv4.conf.all.proxy_arp = 0
net.ipv4.conf.all.arp_filter = 1
net.ipv4.conf.all.arp_announce = 2
net.ipv4.conf.all.arp_ignore = 1
net.ipv4.conf.all.arp_accept = 0
net.ipv4.conf.all.arp_notify = 1
net.ipv4.conf.all.rp_filter = 1
net.ipv4.conf.default.proxy_arp = 0
net.ipv4.conf.default.arp_filter = 1
net.ipv4.conf.default.arp_announce = 2
net.ipv4.conf.default.arp_ignore = 1
net.ipv4.conf.default.arp_accept = 0
net.ipv4.conf.default.arp_notify = 1
net.ipv4.conf.default.rp_filter = 1

#########################################################
# workaround for on-line reconfigure
# interfaces as pppX/wds
#########################################################
net.ipv4.ip_dynaddr = 1
net.ipv4.ip_nonlocal_bind = 1

#########################################################
# local connections and net
# decrease netdev_budget for more fast rings free
#########################################################
net.core.somaxconn = 512
net.core.netdev_budget = 128
net.core.dev_weight = 128
net.core.netdev_max_backlog = 30000
net.core.netdev_tstamp_prequeue = 1
net.unix.max_dgram_qlen = 256
net.core.warnings = 0

#########################################################
# set default qdisc to codel
#########################################################
net.core.default_qdisc = fq_codel

#########################################################
# conntrack tune
#########################################################
# for deflecting SYN+ACK or ACK flood attack
net.netfilter.nf_conntrack_tcp_loose = 0
# optimizations
net.netfilter.nf_conntrack_checksum = 0
net.netfilter.nf_conntrack_tcp_no_window_check = 1
net.netfilter.nf_conntrack_generic_timeout = 480
net.netfilter.nf_conntrack_icmp_timeout = 30
net.netfilter.nf_conntrack_tcp_timeout_close = 30
net.netfilter.nf_conntrack_tcp_timeout_close_wait = 40
net.netfilter.nf_conntrack_tcp_timeout_established = 1200
net.netfilter.nf_conntrack_tcp_timeout_fin_wait = 80
net.netfilter.nf_conntrack_tcp_timeout_last_ack = 30
net.netfilter.nf_conntrack_tcp_timeout_syn_sent = 120
net.netfilter.nf_conntrack_tcp_timeout_time_wait = 50
net.netfilter.nf_conntrack_udp_timeout = 50
net.netfilter.nf_conntrack_udp_timeout_stream = 120
net.netfilter.nf_conntrack_tcp_be_liberal = 1

