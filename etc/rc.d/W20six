#!/bin/sh

# if ipv6 not support in kernel
if [ ! -d /proc/sys/net/ipv6 ]; then
    exit 0
fi

#get params
. /etc/scripts/global.sh

if [ "$OperationMode" = "0" ]; then
    exit 0
fi

# prevent double start reconfigure ipv6 in one time
while [ -e /tmp/reconfigure_six_runing ]; do
    # Sleep until file does exists/is created
    usleep 500000
done
echo $$ > /tmp/reconfigure_six_runing

LOG="logger -t ipv6"

calculate_and_set_mtu() {
    # calculate and set ipv6 mtu for correct of full avoid fragmentation
    if [ "$IPv6OpMode" = "1" ]; then
	if [ "$IPv6ManualMTU" != "" ] && [ "$IPv6ManualMTU" != "0" ]; then
	    # set manual MTU to WAN and as defaults
	    wanmtu="$IPv6ManualMTU"
	    sysctl -w net.ipv6.conf.$real_wan_if.mtu="$wanmtu"
	    sysctl -w net.ipv6.conf.all.mtu="$wanmtu"
	    sysctl -w net.ipv6.conf.default.mtu="$wanmtu"
	else
	    # get mtu accepted by RA from wan
	    wanmtu=`sysctl -n net.ipv6.conf.$real_wan_if.mtu`
	fi
	# in native move LAN MTU=WAN MTU for avoid fragmentation
    	ipvmtu=$wanmtu
    else
	# set defult mtu for tunneled mode
	sysctl -w net.ipv6.conf.all.mtu=1280
	sysctl -w net.ipv6.conf.default.mtu=1280
	# get mtu from wan
	wanmtu=`ip -o -4 link show dev "$tun_wan_if" | awk {' print $5 '}`
	if [ "$wanmtu" = "" ]; then
	    #min of max for tun at wan
	    wanmtu="1460"
	fi
	# calculate mtu
	let "sitmtu=$wanmtu - 20"
	let "ipvmtu=$sitmtu - 40"
    fi
    # set mtu for lan
    sysctl -wq net.ipv6.conf."$lan_if".mtu="$ipvmtu"
    $LOG "MTU WAN: $wanmtu LAN: $ipvmtu"
}

start() {
    get_param
    if [ "$IPv6OpMode" != "" ] && [ "$IPv6OpMode" != "0" ]; then
	# load tun module support for 6RD/6TO4
	if [ "$IPv6OpMode" = "2" ] || [ "$IPv6OpMode" = "3" ]; then
	    modprobe -q sit
	fi
	# addr del->add change need for notify kernel and regenerage arp table
        if [ "$IPv6OpMode" = "1" ]; then
	    # this section for native/dualstack mode
	    if [ "$IPv6Dhcpc" = "1" ]; then
		# call dhcpv6 client over real wan only if gw v6 not in vpn or vpn disabled
		if [ "$vpnEnabled" != "on" ] || [ "$Ipv6InVPN" != "1" ]; then
	    	    dhcpcreconf
		fi
	    else
    		$LOG "Native IPv6 support static configured. Disable router advertisements for wan $six_wan_if"
		sysctl -wq net.ipv6.conf.$six_wan_if.accept_ra=0

                $LOG "Set ${IPv6WANIPAddr}/${IPv6WANPrefixLen} to $six_wan_if"
		ip -6 addr del ${IPv6WANIPAddr}/${IPv6WANPrefixLen} dev $six_wan_if			> /dev/null 2>&1
		ip -6 addr add ${IPv6WANIPAddr}/${IPv6WANPrefixLen} dev $six_wan_if

                $LOG "Set ${IPv6IPAddr}/${IPv6PrefixLen} to $lan_if"
		ip -6 addr del ${IPv6IPAddr}/${IPv6PrefixLen} dev $lan_if				> /dev/null 2>&1
		ip -6 addr add ${IPv6IPAddr}/${IPv6PrefixLen} dev $lan_if

		$LOG "Replace Ipv6 DGW to $six_wan_if via $IPv6GWAddr"
		ip -6 route replace default dev "$six_wan_if" via "$IPv6GWAddr" metric 0
		flush_net_caches
		dhcpradvdreconf
	    fi
        elif [ "$IPv6OpMode" = "2" ]; then
	    # this section for 6rd tunnel mode
    	    $LOG "Tunel ipv6 in ipv4 configure and up at $six_wan_if interface"
	    ip tunnel add $six_wan_if mode sit local $tun_wan_ipaddr ttl 64
	    ip tunnel 6rd dev $six_wan_if 6rd-prefix ${ipv6prefix}/${IPv6PrefixLen} 6rd-relay_prefix 0
	    ip link set $six_wan_if up
	    $LOG "Add adress ${ipv6prefix}/${IPv6PrefixLen} to $six_wan_if"
	    ip addr del ${ipv6prefix}/${IPv6PrefixLen} dev 6rd						> /dev/null 2>&1
	    ip addr add ${ipv6prefix}/${IPv6PrefixLen} dev 6rd
        elif [ "$IPv6OpMode" = "3" ]; then
	    # this section for 6to4 tunnel mode
    	    $LOG "Tunel ipv6 to ipv4 configure and up at $six_wan_if interface"
	    ip -6 link set $six_wan_if up
	    $LOG "Add adress ${IPv6IPAddr}:${ipv6prefix}::1/16 to $six_wan_if"
	    ip -6 addr del ${IPv6IPAddr}:${ipv6prefix}::1/16 dev $six_wan_if						> /dev/null 2>&1
	    ip -6 addr add ${IPv6IPAddr}:${ipv6prefix}::1/16 dev $six_wan_if
	fi

	if [ "$IPv6OpMode" = "2" ] || [ "$IPv6OpMode" = "3" ]; then
	    # tune after tunnels configured
	    calculate_and_set_mtu # Calculate and set mtu
	    $LOG "Set mtu $sitmtu to $six_wan_if"
	    sysctl -wq net.ipv6.conf.$six_wan_if.mtu="$sitmtu"
	    ip link set mtu $sitmtu dev $six_wan_if
	    # add direct default gateway for workaround No route to host on new kernel
            ip -6 route replace default dev $six_wan_if metric 2048
	    $LOG "Add route to 2000::/3 via ::${relay6to4} dev $six_wan_if"
	    ip -6 route replace 2000::/3 via ::${relay6to4} dev $six_wan_if metric 1
	    $LOG "Used tunneled ipv6 mode, disable forward from real wan interfaces (security reason), enable foe $six_wan_if."
    	    sysctl -wq net.ipv6.conf.$tun_wan_if.forwarding=0
    	    sysctl -wq net.ipv6.conf.$wan_if.forwarding=0
    	    sysctl -wq net.ipv6.conf.$six_wan_if.forwarding=1
	    $LOG "Used tunneled ipv6 mode, not need ipv6 adress and route at wan ifaces - drop it (security reason)."
	    ip -6 route flush dev $wan_if								> /dev/null 2>&1
	    ip -6 route flush dev $tun_wan_if								> /dev/null 2>&1
	    ip -6 addr flush dev $wan_if								> /dev/null 2>&1
	    ip -6 addr flush dev $tun_wan_if								> /dev/null 2>&1
	    $LOG "Set ${PREFIX} to $lan_if"
	    ip -6 addr del ${PREFIX} dev $lan_if							> /dev/null 2>&1
	    ip -6 addr add ${PREFIX} dev $lan_if
	    flush_net_caches
	    dhcpradvdreconf
	fi
    fi
}

dhcpradvdreconf() {
    get_param
    if [ "$IPv6OpMode" != "" ] && [ "$IPv6OpMode" != "0" ]; then
	$LOG "Configure and start radvd and dhcpv6 server"

	# get prefix direct from lan iface if not set
	if [ "$PREFIX" = "" ]; then
	    $LOG "Try get prefix from local interface $lan_if $PREFIX"
	    PREFIX=`ip -6 -o addr show scope global dev $lan_if | awk {' print $4 '}`
	    if [ "$PREFIX" = "" ]; then
		$LOG "Prefix empty, stop reconfig!"
		return
	    fi
	fi

	# use local dns server as relay
	if [ "$dnsPEnabled" = "1" ]; then
	    DNSADRESSES=`ip -o -6 addr show dev $lan_if scope global | awk {' print $4 '} | cut -f1 -d"/"`
	    $LOG "Use local DNS server as relay and local ipv6 hosts resolve $DNSADRESSES"
	else
	    # sort and uniq dns
	    uniqdns
	    # dns servers for clients if local dns relay is off
	    if [ -e /etc/resolvipv6.conf ] && [ "$IPv6Dhcpc" = "1" ]; then
		DNSADRESSES=`echo $(cat /etc/resolvipv6.conf | grep nameserver | grep : | grep -v "nameserver ::1" |  sed -e 's/nameserver//g')`
		$LOG "Use DNS recieved from ISP $DNSADRESSES"
	    fi

	    if [ "$DNSADRESSES" = "" ] && [ "$IPv6DNSPrimary" != "" -o "$IPv6DNSSecondary" != "" ]; then
		DNSADRESSES="$IPv6DNSPrimary $IPv6DNSSecondary"
		$LOG "Use user manual configured DNS $DNSADRESSES"
	    fi

	    if [ "$DNSADRESSES" = "" ]; then
		DNSADRESSES="2001:4860:4860::8888 2001:4860:4860::8844"
		$LOG "Use public google DNS $DNSADRESSES"
	    fi
	fi

	if [ "$dhcpv6Enabled" = "1" ]; then
	    # use DHCPv6 for get DNS server adresses, need for Windows clients compat
	    AdvOtherConfigFlag="on"
	else
	    AdvOtherConfigFlag="off"
	fi

	calculate_and_set_mtu # ReCalculate and set mtu (after every dhcp6c renew or manual reconf)

	if [ "$radvdEnabled" = "1" ]; then
	    $LOG "Configure radvd"
    	    printf "
    	    interface $lan_if
            {
                AdvSendAdvert on;
                MinRtrAdvInterval 3;
                MaxRtrAdvInterval 10;
		AdvLinkMTU $ipvmtu;		# send correct MTU size for avoid fragment on router
                AdvDefaultPreference high;
                AdvManagedFlag off;		# use RADVD for clients adresses set
                AdvOtherConfigFlag $AdvOtherConfigFlag;
            	prefix ${PREFIX}
            	{
            	    AdvOnLink off;
                    AdvAutonomous on;
                    AdvRouterAddr on;
                    AdvPreferredLifetime 240;
                    AdvValidLifetime 300;
    		" > /etc/radvd.conf
		if [ "$IPv6OpMode" = "2" ] || [ "$IPv6OpMode" = "3" ]; then
		    if [ "${IPv6ISPPrefix}" = "" ] || [ "${IPv6ISPPrefix}" = "off" ]; then
                        printf "    Base6to4Interface $tun_wan_if;" >> /etc/radvd.conf
		    fi
		fi
    		printf "
            	};

                RDNSS $DNSADRESSES
                {
                        AdvRDNSSLifetime 20;
                };
            };
    	    " >> /etc/radvd.conf

	    pid=`pidof radvd`
	    if [ "$pid" != "" ]; then
		killall -q -SIGHUP radvd
	    else
		$LOG "Starting radvd"
		# force remove pid file if process not exists
		rm -f /var/run/radvd.pid
		radvd -C /etc/radvd.conf --debug=0 --logmethod=$logto &
	    fi
	fi

	if [ "$dhcpv6Enabled" = "1" ]; then
	    $LOG "Configure dhcp6s"
	    printf "
	    option domain-name-servers $DNSADRESSES;

	    interface $lan_if {
		allow rapid-commit;
	    };
	    " > /etc/dhcp6s.conf

	    pid=`pidof dhcp6s`
	    if [ "$pid" != "" ]; then
		killall -q -SIGHUP dhcp6s
	    else
		$LOG "Starting dhcp6s"
		# force remove pid file if process not exists
		rm -f /var/run/dhcp6s.pid
		dhcp6s -c /etc/dhcp6s.conf -P /var/run/dhcp6s.pid $lan_if &
	    fi
    	fi
    fi
}

dhcpradvdstop() {
    pid=`pidof radvd`
    if [ "$pid" != "" ]; then
	$LOG  "Stop radvd."
	# terminate radvd daemon
	while killall -q radvd; do
	    usleep 500000
	    killall -q -SIGKILL radvd
	done
	# force remove pid file if process killed
	rm -f /var/run/radvd.pid
    fi
    pid=`pidof dhcp6s`
    if [ "$pid" != "" ]; then
	$LOG  "Stop dhcp6s."
	# terminate dhcp6s daemon
	while killall -q dhcp6s; do
	    usleep 500000
	    killall -q -SIGKILL dhcp6s
	done
	# force remove pid file if process killed
	rm -f /var/run/dhcp6s.pid
    fi
}

dhcpcreconf() {
    dhcpcstop

    if [ -e /bin/dhcp6c ] && [ "$IPv6OpMode" = "1" ] && [ "$IPv6Dhcpc" = "1" ]; then
	$LOG "Native IPv6 support with DHCP/RA uplink. Always accept router advertisements for autoconfigure from wan $six_wan_if and forward enable."
	# ipv6 mode preconfig for wan
	sysctl -wq net.ipv6.conf.$six_wan_if.disable_ipv6=0
	sysctl -wq net.ipv6.conf.$six_wan_if.forwarding=1

	# allow autoconfigure by RA
	sysctl -wq net.ipv6.conf.$six_wan_if.accept_ra=2
	sysctl -wq net.ipv6.conf.$six_wan_if.accept_ra_pinfo=1
	sysctl -wq net.ipv6.conf.$six_wan_if.autoconf=1

	# remove pregenerated duid
	rm -f /etc/dhcp6c_duid

	# generate dhcpv6 client config (use mac from WAN)
	ID=`ip -o link show dev $wan_if | awk {' print $15 '}  | awk -F":" ' { print  "0x"$4$5$6 } '`
	ID=`printf "%d" "$ID"`

	# tune some parametrs between modes
	# IPOE dualstack - reqest IA-NA/PD
	# PPTP/L2TP/PPPOE dualstack - do not reqest IA-NA, only PD
	if [ "$vpnEnabled" != "on" ] || [ "$Ipv6InVPN" != "1" ]; then
	    # force disable ia-na from user config
	    if [ "$IPv6DisableIANA" != "1" ]; then
		REQIANA="send ia-na $ID;"
	    fi
	fi
	SLAID="sla-id 0;"
	if [ "$IPv6SlaLen" = "" ]; then
	    SLALEN="sla-len 16;"
	else
	    SLALEN="sla-len $IPv6SlaLen;"
	fi

	printf "
	interface $six_wan_if {
	    send ia-pd $ID;
	    $REQIANA
	    send rapid-commit;
	    request domain-name-servers;
	    script \"/etc/scripts/dhcp6c-script\";
	};
	id-assoc pd $ID {
	    prefix-interface $lan_if {
		$SLAID
		$SLALEN
	    };
	};
	id-assoc na $ID { };
	" > /etc/dhcp6c.conf

	$LOG "Start DHCPv6 client at $six_wan_if."
	dhcp6c $six_wan_if > /dev/null 2>&1
    fi
}

dhcpcstop() {
    pid=`pidof dhcp6c`
    if [ "$pid" != "" ]; then
	$LOG  "Stop dhcp6c."
	# terminate dhcp6c daemon
	while killall -q dhcp6c; do
    	    # wait release procedure
	    sleep 1
	    killall -q -SIGKILL dhcp6c
	done
    fi
}

get_param() {
    eval `nvram_buf_get 2860 radvdEnabled dhcpv6Enabled IPv6IPAddr IPv6SrvAddr IPv6WANIPAddr IPv6PrefixLen IPv6WANPrefixLen \
	    IPv6GWAddr IPv6DNSPrimary IPv6DNSSecondary IPv6ManualMTU IPv6DisableIANA IPv6ISPPrefix IPv6SlaLen`

    if [ "$SysLogd" = "1" ]; then
	logto="syslog"
    else
	logto="stderr"
    fi

    if [ "$IPv6OpMode" != "" ] && [ "$IPv6OpMode" != "0" ]; then
	# switch tun over wan/vpn
	if [ "$Ipv6InVPN" = "1" ]; then
	    tun_wan_if="$real_wan_if"
	    tun_wan_ipaddr="$real_wan_ipaddr"
	else
	    tun_wan_if="$wan_if"
	    tun_wan_ipaddr="$wan_ipaddr"
	fi

	# check wan ip set, need v4 adress at wan for tunnels config
	if [ "$IPv6OpMode" != "1" ] && [ "$tun_wan_ipaddr" = "" -o "$IPv6SrvAddr" = "" ]; then
    	    $LOG "No ipv4 adress on wan or tunnel adress not set (need for tunnel mode), EXIT!"
	    # remove running flag
	    rm -f /tmp/reconfigure_six_runing
    	    exit 1
	fi

        # enable ipv6 and forwarding
        sysctl -wq net.ipv6.conf.all.disable_ipv6=0
        sysctl -wq net.ipv6.conf.all.forwarding=1
        sysctl -wq net.ipv6.conf.all.accept_ra=1
        sysctl -wq net.ipv6.conf.all.accept_ra_pinfo=1
        sysctl -wq net.ipv6.conf.all.autoconf=1

        # 6RD mode
        if [ "$IPv6OpMode" = "2" ]; then
	    tun_wan_ipaddr_nodots=`echo ${tun_wan_ipaddr} | tr . ' '`                                           # Remove dots form ipv4 WAN adress
	    v4prefix=`ip -o -4 addr show dev "$tun_wan_if" scope global | awk {' print $4 '}`			# Get ip/metmask from if for use as src
	    relay6to4="$IPv6SrvAddr"										# The 6rd IPv4-gateway
	    ipv6prefix="$IPv6IPAddr"										# Your ISP's IPv6-prefix
	    PREFIX="${ipv6prefix}2/${IPv6PrefixLen}"								# Prefix with first lan addr 2 for RA announce

	    #tun_wan_ipaddr_nodots=`echo ${tun_wan_ipaddr} | tr . ' '`                                           # Remove dots form ipv4 WAN adress
	    #six_tun_ip=`printf "${ipv6prefix}:%02x%02x:%02x%02x::1" ${tun_wan_ipaddr_nodots}`			 # Calculate local ipv6 address for tunnel from real ipv4 adress
	    #lan_subnet=`printf "%02x%02x:%02x%02x::2" ${tun_wan_ipaddr_nodots}`				 # Calculate subnet from real ipv4 adress
	    #PREFIX="${ipv6prefix}:${lan_subnet}/${IPv6PrefixLen}"

        # 6TO4 mode
        elif [ "$IPv6OpMode" = "3" ]; then
            ipv6prefix=`echo $tun_wan_ipaddr | awk -F. '{ printf "%02x%02x:%02x%02x", $1, $2, $3, $4 }'`        # Calculate prefix
            relay6to4="$IPv6SrvAddr"                                                                            # Manual 6to4 IPv4-gateway
	    if [ "${IPv6ISPPrefix}" = "" ] || [ "${IPv6ISPPrefix}" = "off" ]; then
		IPv6IPAddr="2002"
		IPv6PrefixLen="64"
		PREFIX="${IPv6IPAddr}:${ipv6prefix}:2000::1/${IPv6PrefixLen}"
	    else
		PREFIX="${IPv6IPAddr}:${ipv6prefix}::2/${IPv6PrefixLen}"
	    fi
        fi
    fi
}

stop() {
    dhcpcstop
    dhcpradvdstop

    $LOG  "Deconfigure ipv6 interfaces/route."

    # always clear conntrack and routes tables/caches
    flush_net_caches

    # disable ra for all ifaces
    sysctl -wq net.ipv6.conf.all.accept_ra=0		> /dev/null 2>&1
    sysctl -wq net.ipv6.conf.all.forwarding=0		> /dev/null 2>&1
    sysctl -wq net.ipv6.conf.6rd.forwarding=0		> /dev/null 2>&1
    sysctl -wq net.ipv6.conf.sit0.forwarding=0		> /dev/null 2>&1
    sysctl -wq net.ipv6.conf.$lan_if.forwarding=0	> /dev/null 2>&1
    sysctl -wq net.ipv6.conf.$tun_wan_if.forwarding=0	> /dev/null 2>&1
    sysctl -wq net.ipv6.conf.$six_wan_if.forwarding=0	> /dev/null 2>&1

    # disable ipv6
    sysctl -wq net.ipv6.conf.all.disable_ipv6=1

    # cleanup global adresses
    echo "#!/bin/sh" > /tmp/ip6addrdel
    ip -6 -o addr show scope global | awk {' print "ip -6 addr del " $4 " dev " $2 '} >> /tmp/ip6addrdel
    chmod 755 /tmp/ip6addrdel
    sh /tmp/ip6addrdel
    rm -f /tmp/ip6addrdel

    ip -6 route flush scope all dev 6rd				> /dev/null 2>&1
    ip -6 route flush scope all dev sit0			> /dev/null 2>&1
    ip -6 route flush scope all dev $tun_wan_if			> /dev/null 2>&1
    ip -6 route flush scope all dev $six_wan_if			> /dev/null 2>&1
    ip link set dev 6rd down					> /dev/null 2>&1
    ip link set dev sit0 down					> /dev/null 2>&1
    ip tunnel del 6rd						> /dev/null 2>&1

    # always remove sit module if auto loaded after call ip tool
    rmmod sit > /dev/null 2>&1

    # always clear conntrack and routes tables/caches
    flush_net_caches
}

case "$1" in
	start)
	    start
	    ;;

	stop)
	    stop
	    ;;

	dhcpradvdreconf)
	    dhcpradvdreconf
	    ;;

	dhcpradvdstop)
	    dhcpradvdstop
	    ;;

	dhcpcreconf)
	    dhcpcreconf
	    ;;

	dhcpcstop)
	    dhcpcstop
	    ;;

	restart)
	    stop
	    start
	    ;;

	*)
	    echo $"Usage: $0 {start|stop|dhcpradvdreconf|dhcpradvdstop|dhcpcreconf|dhcpcstop|restart}"
esac

# remove running flag
rm -f /tmp/reconfigure_six_runing
